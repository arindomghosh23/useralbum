package com.example.useralbum.network

import com.example.useralbum.network.model.Album
import com.example.useralbum.network.model.Photo
import com.example.useralbum.network.model.User
import com.example.useralbum.ui.album_screen.model.AlbumLocal
import com.example.useralbum.ui.photo_screen.model.PhotoLocal
import com.example.useralbum.ui.user_screen.model.UserLocal
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object AppApiHandler {

    private val mUserAlbumServices = RetrofitBuilder.createImageSearchServices()

    fun getUsers(listener: GenericApiListener<List<UserLocal>, String?>) {
        mUserAlbumServices.getUsers()
            .enqueue(object : Callback<List<User>> {

                override fun onResponse(
                    call: Call<List<User>>,
                    response: Response<List<User>>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        val users = mutableListOf<UserLocal>()
                        response.body()?.forEach { users.add(UserLocal(it.id, it.name, it.email)) }
                        listener.onSuccess(users)
                    } else {
                        listener.onFailure("Something went wrong")
                    }
                }

                override fun onFailure(call: Call<List<User>>, t: Throwable) {
                    listener.onFailure(t.message ?: "Service Issue")
                }

            })
    }

    fun getAlbumsForUsers(userId: Int, listener: GenericApiListener<List<AlbumLocal>, String>) {
        mUserAlbumServices.getAlbumForService(userId)
            .enqueue(object : Callback<List<Album>> {

                override fun onResponse(
                    call: Call<List<Album>>,
                    response: Response<List<Album>>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        val albums = mutableListOf<AlbumLocal>()
                        response.body()
                            ?.forEach { albums.add(AlbumLocal(it.userID, it.id, it.title)) }
                        listener.onSuccess(albums)
                    } else {
                        listener.onFailure("Something went wrong")
                    }
                }

                override fun onFailure(call: Call<List<Album>>, t: Throwable) {
                    listener.onFailure(t.message ?: "Service Issue")
                }

            })
    }


    fun getPhotoesforAlbum(albumId: Int, listener: GenericApiListener<List<PhotoLocal>, String>) {
        mUserAlbumServices.getPhotoesForAlbumId(albumId)
            .enqueue(object : Callback<List<Photo>> {

                override fun onResponse(
                    call: Call<List<Photo>>,
                    response: Response<List<Photo>>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        val mPhotos = mutableListOf<PhotoLocal>()
                        response.body()?.forEach {
                            mPhotos.add(
                                PhotoLocal(
                                    it.albumID,
                                    it.id,
                                    it.title,
                                    it.url,
                                    it.thumbnailUrl
                                )
                            )
                        }
                        listener.onSuccess(mPhotos)
                    } else {
                        listener.onFailure("Something went wrong")
                    }
                }

                override fun onFailure(call: Call<List<Photo>>, t: Throwable) {
                    listener.onFailure(t.message ?: "Service Issue")
                }

            })
    }
}