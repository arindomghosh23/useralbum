package com.example.useralbum.network.model

data class Photo(
    val albumID: Int,
    val id: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)
