package com.example.useralbum.network

import com.example.useralbum.network.model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface UserAlbumServices {

    @GET("/users")
    fun getUsers(): Call<List<User>>

    @GET("/albums")
    fun getAlbumForService(@Query("userId") userId: Int): Call<List<Album>>

    @GET("/photos")
    fun getPhotoesForAlbumId(@Query("albumId") albumId: Int):Call<List<Photo>>
}