package com.example.useralbum.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    private fun retrofitBuilder(baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun createImageSearchServices(): UserAlbumServices =
        retrofitBuilder("https://jsonplaceholder.typicode.com/").create(UserAlbumServices::class.java)

}