package com.example.useralbum.network.model


data class Album(

    val userID: Int,
    val id: Int,
    val title: String
)