package com.example.useralbum.ui.album_screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.useralbum.R
import com.example.useralbum.ui.GenericCallback
import com.example.useralbum.ui.album_screen.model.AlbumLocal
import kotlinx.android.synthetic.main.item_album_details.view.*

class AlbumDetailsAdapter(
    private var mAlbums: List<AlbumLocal>,
    private val listener: GenericCallback<AlbumLocal>
) :
    RecyclerView.Adapter<AlbumDetailsAdapter.UserDataViewHolder>() {

    inner class UserDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(mAlbum: AlbumLocal) {
            itemView.tv_album_name.text = mAlbum.title
            itemView.setOnClickListener { listener.OnItemSelected(mAlbum) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = UserDataViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_album_details,
            parent,
            false
        )
    )


    override fun getItemCount() = mAlbums.size

    override fun onBindViewHolder(holder: UserDataViewHolder, position: Int) {
        holder.bind(mAlbums[position])
    }

    fun updateView(albumList: List<AlbumLocal>) {
        this.mAlbums = albumList
        notifyDataSetChanged()
    }
}