package com.example.useralbum.ui

interface GenericCallback<in T> {
    fun OnItemSelected(userInput: T)
}