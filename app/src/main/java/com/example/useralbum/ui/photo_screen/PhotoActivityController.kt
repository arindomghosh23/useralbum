package com.example.useralbum.ui.photo_screen

import com.example.useralbum.common.ActivityControllers
import com.example.useralbum.network.AppApiHandler
import com.example.useralbum.network.GenericApiListener
import com.example.useralbum.ui.photo_screen.model.PhotoLocal

class PhotoActivityController : ActivityControllers<PhotoActivity> {
    private var mPhotoActivity: PhotoActivity? = null
    override fun create(mActivity: PhotoActivity) {
        mPhotoActivity = mActivity
    }

    fun getPhotoesForAlbum(albumId: Int){
        AppApiHandler.getPhotoesforAlbum(albumId, object :GenericApiListener<List<PhotoLocal>,String?>{
            override fun onSuccess(message: List<PhotoLocal>) {
                mPhotoActivity?.updateUser(message)
            }

            override fun onFailure(message: String?) {
                mPhotoActivity?.showFailedMessage(message?:"Something went wrong")
            }
        })
    }

    override fun destroy() {
        mPhotoActivity = null
    }
}