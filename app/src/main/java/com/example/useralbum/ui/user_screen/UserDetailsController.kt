package com.example.useralbum.ui.user_screen

import com.example.useralbum.common.ActivityControllers
import com.example.useralbum.network.AppApiHandler
import com.example.useralbum.network.GenericApiListener
import com.example.useralbum.ui.user_screen.model.UserLocal

class UserDetailsController : ActivityControllers<UserDetailsActivity> {
    private var mUserDetailsActivity: UserDetailsActivity? = null

    override fun create(mActivity: UserDetailsActivity) {
        mUserDetailsActivity = mActivity
    }

    fun getUsers(){
        AppApiHandler.getUsers(object : GenericApiListener<List<UserLocal>, String?>{
            override fun onSuccess(message: List<UserLocal>) {
                mUserDetailsActivity?.updateUser(message)
            }

            override fun onFailure(message: String?) {
                mUserDetailsActivity?.showFailedMessage(message?:"Somwthing went wrong")
            }
        })
    }

    override fun destroy() {
        mUserDetailsActivity = null
    }
}