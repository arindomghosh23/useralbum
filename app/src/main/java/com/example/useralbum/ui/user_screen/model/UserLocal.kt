package com.example.useralbum.ui.user_screen.model

data class UserLocal(
    val Id: Int,
    val name: String,
    val email: String
)
