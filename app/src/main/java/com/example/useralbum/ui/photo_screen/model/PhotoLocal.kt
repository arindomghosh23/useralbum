package com.example.useralbum.ui.photo_screen.model

data class PhotoLocal(
    val albumId: Int,
    val photoId: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)