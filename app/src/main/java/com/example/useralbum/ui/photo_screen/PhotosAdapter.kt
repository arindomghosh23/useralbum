package com.example.useralbum.ui.photo_screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.useralbum.R
import com.example.useralbum.ui.GenericCallback
import com.example.useralbum.ui.photo_screen.model.PhotoLocal
import com.example.useralbum.utils.ImageLoader
import kotlinx.android.synthetic.main.item_photo_details.view.*

class PhotosAdapter(
    private val mImageLoader: ImageLoader,
    private var mPhotos: List<PhotoLocal>,
    private val listener: GenericCallback<PhotoLocal>
) :
    RecyclerView.Adapter<PhotosAdapter.UserDataViewHolder>() {

    inner class UserDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(mPhoto: PhotoLocal) {
            itemView.tv_photo_name.text = mPhoto.title
            mImageLoader.loadImageFromURL(mPhoto.thumbnailUrl,itemView.iv_photos)
            itemView.setOnClickListener { listener.OnItemSelected(mPhoto) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = UserDataViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_photo_details,
            parent,
            false
        )
    )


    override fun getItemCount() = mPhotos.size

    override fun onBindViewHolder(holder: UserDataViewHolder, position: Int) {
        holder.bind(mPhotos[position])
    }

    fun updateView(albumList: List<PhotoLocal>) {
        this.mPhotos = albumList
        notifyDataSetChanged()
    }
}