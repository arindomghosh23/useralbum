package com.example.useralbum.ui.album_screen.model

data class AlbumLocal(
    val userid: Int,
    val albumId: Int,
    val title: String
)