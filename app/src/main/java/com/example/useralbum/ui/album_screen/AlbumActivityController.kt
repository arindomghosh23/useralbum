package com.example.useralbum.ui.album_screen

import com.example.useralbum.common.ActivityControllers
import com.example.useralbum.network.AppApiHandler
import com.example.useralbum.network.GenericApiListener
import com.example.useralbum.ui.album_screen.model.AlbumLocal
import retrofit2.Call

class AlbumActivityController : ActivityControllers<AlbumActivity> {
    private var mAlbumActivity: AlbumActivity? = null
    override fun create(mActivity: AlbumActivity) {
        mAlbumActivity = mActivity
    }

    fun getAlbumFOrTheUser(mUserId:Int){
        AppApiHandler.getAlbumsForUsers(mUserId,object :GenericApiListener<List<AlbumLocal>, String?>{
            override fun onSuccess(message: List<AlbumLocal>) {
                mAlbumActivity?.updateUser(message)
            }

            override fun onFailure(message: String?) {
                mAlbumActivity?.showFailedMessage(message?:"Something went wrong")
            }
        })
    }

    override fun destroy() {
        mAlbumActivity = null
    }
}