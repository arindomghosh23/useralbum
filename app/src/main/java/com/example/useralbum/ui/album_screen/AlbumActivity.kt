package com.example.useralbum.ui.album_screen

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.useralbum.R
import com.example.useralbum.common.BaseActivity
import com.example.useralbum.ui.GenericCallback
import com.example.useralbum.ui.album_screen.model.AlbumLocal
import com.example.useralbum.ui.photo_screen.PhotoActivity
import kotlinx.android.synthetic.main.activity_album.*

class AlbumActivity : BaseActivity() {
    private var mAlbumActivityController: AlbumActivityController? = null
    private lateinit var mAlbumDetailsAdapter: AlbumDetailsAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var mUserId: Int = 0
    private val mListener = object : GenericCallback<AlbumLocal> {
        override fun OnItemSelected(userInput: AlbumLocal) {
            val mBundle = Bundle()
            mBundle.putInt("AlbumId", userInput.albumId)
            val photoIntent = Intent(this@AlbumActivity, PhotoActivity::class.java)
            photoIntent.putExtras(mBundle)
            startActivity(photoIntent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_album)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        mAlbumActivityController = AlbumActivityController()
        mUserId= intent.extras?.getInt("UserId")!!
        setAdapter()
    }

    override fun onStart() {
        super.onStart()
        mAlbumActivityController?.create(this)
        mAlbumActivityController?.getAlbumFOrTheUser(mUserId)
    }


    private fun setAdapter() {
        mAlbumDetailsAdapter = AlbumDetailsAdapter(emptyList(), mListener)
        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rv_albums.layoutManager = linearLayoutManager
        rv_albums.setHasFixedSize(true)
        rv_albums.adapter = mAlbumDetailsAdapter
    }

    fun updateUser(users: List<AlbumLocal>) {
        progress_circular.visibility = View.GONE
        mAlbumDetailsAdapter.updateView(users)
    }

    fun showFailedMessage(message: String) {
        progress_circular.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onStop() {
        super.onStop()
        mAlbumActivityController?.destroy()
    }

    override fun onDestroy() {
        super.onDestroy()
        mAlbumActivityController = null
    }
}
