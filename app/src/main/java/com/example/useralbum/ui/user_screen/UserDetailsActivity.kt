package com.example.useralbum.ui.user_screen

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.useralbum.R
import com.example.useralbum.common.BaseActivity
import com.example.useralbum.ui.GenericCallback
import com.example.useralbum.ui.album_screen.AlbumActivity
import com.example.useralbum.ui.user_screen.model.UserLocal
import kotlinx.android.synthetic.main.user_details_activity.*

class UserDetailsActivity : BaseActivity() {
    private lateinit var mUserDetailsAdapter: UserDetailsAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var mUserDetailsController: UserDetailsController? = null
    private val mListener = object : GenericCallback<UserLocal> {
        override fun OnItemSelected(userInput: UserLocal) {
            val mBundle = Bundle()
            mBundle.putInt("UserId", userInput.Id)
            val albumIntent = Intent(this@UserDetailsActivity, AlbumActivity::class.java)
            albumIntent.putExtras(mBundle)
            startActivity(albumIntent)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.user_details_activity)
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        mUserDetailsController?.create(this)
        mUserDetailsController?.getUsers()
    }

    override fun init() {
        mUserDetailsController = UserDetailsController()
        setAdapter()
    }

    private fun setAdapter() {
        mUserDetailsAdapter = UserDetailsAdapter(emptyList(), mListener)
        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rv_users.layoutManager = linearLayoutManager
        rv_users.setHasFixedSize(true)
        rv_users.adapter = mUserDetailsAdapter
    }


    fun updateUser(users: List<UserLocal>) {
//        println("users ${users.size}")
        progress_circular.visibility = View.GONE
        mUserDetailsAdapter.updateView(users)
    }

    fun showFailedMessage(message: String) {
        progress_circular.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onStop() {
        super.onStop()
        mUserDetailsController?.destroy()
    }


    override fun onDestroy() {
        super.onDestroy()
        mUserDetailsController = null
    }
}
