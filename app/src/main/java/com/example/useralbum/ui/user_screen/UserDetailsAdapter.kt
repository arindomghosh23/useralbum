package com.example.useralbum.ui.user_screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.useralbum.R
import com.example.useralbum.ui.GenericCallback
import com.example.useralbum.ui.user_screen.model.UserLocal
import kotlinx.android.synthetic.main.item_user_details.view.*

class UserDetailsAdapter(
    private var mUsers: List<UserLocal>,
    private val listener: GenericCallback<UserLocal>
) :
    RecyclerView.Adapter<UserDetailsAdapter.UserDataViewHolder>() {

    inner class UserDataViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        fun bind(mUser: UserLocal) {
            itemView.tv_user_name.text = mUser.name
            itemView.tv_user_email.text = mUser.email
            itemView.setOnClickListener { listener.OnItemSelected(mUser) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = UserDataViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_user_details,
            parent,
            false
        )
    )


    override fun getItemCount() = mUsers.size

    override fun onBindViewHolder(holder: UserDataViewHolder, position: Int) {
        holder.bind(mUsers[position])
    }

    fun updateView(userList: List<UserLocal>) {
        this.mUsers = userList
        notifyDataSetChanged()
    }
}