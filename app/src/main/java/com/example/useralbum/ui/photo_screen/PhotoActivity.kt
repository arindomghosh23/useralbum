package com.example.useralbum.ui.photo_screen

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.useralbum.R
import com.example.useralbum.common.BaseActivity
import com.example.useralbum.ui.GenericCallback
import com.example.useralbum.ui.photo_screen.model.PhotoLocal
import kotlinx.android.synthetic.main.activity_photo.*

class PhotoActivity : BaseActivity() {
    private var mPhotoActivityController: PhotoActivityController? = null
    private lateinit var mPhotosAdapter: PhotosAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var mAlbumId: Int = 0
    private val mListener = object : GenericCallback<PhotoLocal> {
        override fun OnItemSelected(photoLocal: PhotoLocal) {
            val mBundle = Bundle()
            mBundle.putInt("AlbumId", photoLocal.albumId)

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_photo)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        mPhotoActivityController = PhotoActivityController()
        mAlbumId = intent.extras?.getInt("AlbumId")!!
        setAdapter()
    }

    override fun onStart() {
        super.onStart()
        mPhotoActivityController?.create(this)
        mPhotoActivityController?.getPhotoesForAlbum(mAlbumId)
    }


    private fun setAdapter() {
        mPhotosAdapter = PhotosAdapter(mImageLoader, emptyList(), mListener)
        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rv_photos.layoutManager = linearLayoutManager
        rv_photos.setHasFixedSize(true)
        rv_photos.adapter = mPhotosAdapter
    }

    fun updateUser(photos: List<PhotoLocal>) {
        progress_circular.visibility = View.GONE
        mPhotosAdapter.updateView(photos)
    }

    fun showFailedMessage(message: String) {
        progress_circular.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onStop() {
        super.onStop()
        mPhotoActivityController?.destroy()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPhotoActivityController = null
    }

}

