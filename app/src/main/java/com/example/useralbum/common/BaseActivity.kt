package com.example.useralbum.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.useralbum.utils.ImageLoader

abstract class BaseActivity : AppCompatActivity() {
    protected val mImageLoader by lazy { ImageLoader.getInstance(this) }
    protected abstract fun init()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }
}