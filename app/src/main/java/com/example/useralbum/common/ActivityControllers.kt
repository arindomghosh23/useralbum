package com.example.useralbum.common

interface ActivityControllers<in ActivityContext> {
    fun create(mActivity: ActivityContext)
    fun destroy()
}